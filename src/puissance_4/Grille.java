/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package puissance_4;

import java.util.ArrayList;

/**
 *
 * @author Maxen
 */
public class Grille {
    private ArrayList<ArrayList<Case>> grille = new ArrayList<>();
    private int hauteur,largeur;

    public Grille(int hauteurTailleGrille, int largeurTailleGrille) {
        if (hauteurTailleGrille>5 && largeurTailleGrille>7) {
            hauteur=hauteurTailleGrille;
            largeur = largeurTailleGrille;
            for (int i = 0; i < hauteurTailleGrille; i++) {
                 ArrayList<Case> tmpGrille= new ArrayList<>();
                 for (int j = 0; j < largeurTailleGrille; j++) {
                     tmpGrille.add(new Case(j,i));
                 }
                 grille.add(tmpGrille);
             } 
        }
        else{
            System.out.println("la taille de la grille n'est pas bonne");
        }
        
        
    }
    
    private boolean testLargeur(int x , int y){
        if (x+3<largeur) {
            int rechercher = grille.get(y).get(x).getCase();
            for (int i = 1; i < 4; i++) {
                if (grille.get(y).get(x+i).getCase()!=rechercher) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    private boolean testHauteur(int x , int y){
        if (y+3<hauteur) {
            int rechercher = grille.get(y).get(x).getCase();
            for (int i = 1; i < 4; i++) {
                if (grille.get(y+i).get(x).getCase()!=rechercher) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    private boolean testDiagGD(int x , int y){
//        System.out.println(x +" : "+y);
        if (y+3<hauteur&&x+3<largeur) {
            int rechercher = grille.get(y).get(x).getCase();
            for (int i = 1; i < 4; i++) {
                if (grille.get(y+i).get(x+i).getCase()!=rechercher) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    private boolean testDiagDG(int x , int y){
        if (y+3<hauteur&&x-3>=0) {
            int rechercher = grille.get(y).get(x).getCase();
            for (int i = 1; i < 4; i++) {
                if (grille.get(y+i).get(x-i).getCase()!=rechercher) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public int win(){
        int cpt=0;
        for (int i = 0; i < largeur; i++) {
            if (hauteurDeJeux(i)==hauteur) {
                cpt++;
            }
        }
        if (cpt==largeur) {
            return -1;
        }
        
        for (int i = 0; i < hauteur; i++) {
            for (int j = 0; j < largeur; j++) {
                if (grille.get(i).get(j).getCase()!=0) {
                    if (testLargeur(j, i) || testHauteur(j, i)|| testDiagDG(j, i)|| testDiagGD(j, i)){
                        return grille.get(i).get(j).getCase();
                    }
                }
            }
        }
        return 0;
    }

    public ArrayList<ArrayList<Case>> getGrille() {
        return grille;
    }

    
    
    private int hauteurDeJeux(int x){
        int H=0;
        for (int i = 0; i < hauteur; i++) {
            if (grille.get(i).get(x).getCase()!=0) {
                H++;
            }
        }
        return H;
    }
    
    public boolean setCase(int x,int J){
        if (J==1 || J==2) {
            int y = (grille.size()-1)-hauteurDeJeux(x);
            if (y>=0) {
                grille.get(y).get(x).setCase(J);
                return true;
            }
            return false;
        }
        else{
            return false;
        }
    }

    public int getHauteur() {
        return hauteur;
    }

    public int getLargeur() {
        return largeur;
    }
    
    public boolean tourBot(){
        for (int i = 0; i < hauteur-3; i++) {
            for (int j = 0; j < largeur-3; j++) {
                if (grille.get(i).get(j).getCase()!=2) {
                    
                }
            }
        }
        return true;
    }

    @Override
    public String toString() {
        String tmp="";
        for (int i = 0; i < largeur; i++) {
            tmp+=i;
        }
        tmp+="\n";
        for(ArrayList<Case> ligne : grille){
            for(Case c:ligne){
                tmp+= c.getCase();
            }
            tmp+="\n";
        }
        return tmp;
    }
    
    
    
}
