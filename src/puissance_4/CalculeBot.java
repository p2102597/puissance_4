/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package puissance_4;

import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author Maxen
 */
public class CalculeBot {
    ArrayList<MemBot> memoire = new ArrayList<>();
    
    private void remplissageMemoireHorizontal(Grille grille){
        memoire.clear();
        for (int i = 0; i < grille.getHauteur(); i++) {
            for (int j = 0; j < grille.getLargeur()-3; j++) {
                memoire.add(new MemBot(grille.getGrille().get(i).get(j), grille.getGrille().get(i).get(j+1), grille.getGrille().get(i).get(j+2), grille.getGrille().get(i).get(j+3)));
            }
        }
    }
    
    private void remplissageMemoireVertical(Grille grille){
        memoire.clear();
        for (int i = 0; i < grille.getHauteur(); i++) {
            for (int j = 0; j < grille.getLargeur()-3; j++) {
                memoire.add(new MemBot(grille.getGrille().get(j).get(i), grille.getGrille().get(j+1).get(i), grille.getGrille().get(j+2).get(i), grille.getGrille().get(j+3).get(i)));
            }
        }
    }
    
    private void remplissageMemoireDiageGD(Grille grille){
        memoire.clear();
        for (int i = 0; i < grille.getHauteur()-3; i++) {
            for (int j = 0; j < grille.getLargeur()-3; j++) {
                memoire.add(new MemBot(grille.getGrille().get(i).get(j), grille.getGrille().get(i+1).get(j+1), grille.getGrille().get(i+2).get(j+2), grille.getGrille().get(i+3).get(j+3)));
            }
        }
    }
    
    private void remplissageMemoireDiageDG(Grille grille){
        memoire.clear();
        for (int i = 3; i < grille.getHauteur(); i++) {
            for (int j = 3; j < grille.getLargeur(); j++) {
                memoire.add(new MemBot(grille.getGrille().get(i).get(j), grille.getGrille().get(i-1).get(j-1), grille.getGrille().get(i-2).get(j-2), grille.getGrille().get(i-3).get(j-3)));
            }
        }
    }
    
    private int Max2(int min){
        MemBot maxi = memoire.get(0);
        int indiceMax = min;
        for (int i = min+1; i < memoire.size(); i++) {
            if (memoire.get(i).nbrJ2()>maxi.nbrJ2()) {
                maxi=memoire.get(i);
                indiceMax=i;
            }
        }
        return indiceMax;
    }
    
    private void trieMem2(){
        for (int i = 0; i < memoire.size()-1; i++) {
            
            int iMax = Max2(i);
            
            MemBot memMax = memoire.get(iMax);
            memoire.remove(iMax);
            
            memoire.add(iMax, memoire.get(i));
            memoire.remove(i);
            
            memoire.add(i, memMax);
            
        }
    }
    
    private void removeFULL(){
        for (int i = 0; i < memoire.size(); i++) {
            if (memoire.get(i).nbrVide()==0||memoire.get(i).nbrVide()==4) {
                memoire.remove(i);
            }
        }
    }
    
    private void removeJ1(){
        MemBot tmp = null;
        for (int i = 0; i < memoire.size(); i++) {
            if (memoire.get(i).nbrJ1()!=0) {
                tmp=memoire.get(i);
                memoire.remove(i);
            }
        }
//        if (memoire.isEmpty()) {
//            memoire.add(tmp);
//        }
    }
    
    public Point Horizontal(Grille grille){
        remplissageMemoireHorizontal(grille);
        trieMem2();
        removeFULL();
        removeJ1();
        removeFULL();
        //System.out.println(memoire);
        System.out.println(memoire.size());
        return memoire.get(0).ouJouer();
    }
    
}
