/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package puissance_4;

/**
 *
 * @author Maxen
 */
public class Case {
    private int etat;
    private int x,y;

    public Case(int xx, int yy) {
        etat = 0;
        x=xx;
        y=yy;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    
    
    public int getCase(){
        return etat;
    }
    
    public boolean setCase(int c){
        if (c==1 || c== 2|| c== 0) {
            etat = c;
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public String toString() {
        return getCase()+"";
    }
    
    
}
