/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package puissance_4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Maxen
 */
public class WinFenetre extends JDialog implements ActionListener{
    JLabel Texte;
    JButton rejouer = new JButton("Rejouer");
    Boolean rejouerCondition=false;

    public WinFenetre(MaFenetre fen, int i) {
        super(fen,true);
        
        JPanel pano = new JPanel();
        
        switch (i) {
            case 1 -> Texte = new JLabel("La gagnant est la joueur 1");
            case 2 -> Texte = new JLabel("La gagnant est la joueur 2");
            case -1 -> Texte = new JLabel("Egalite");
            default -> {
            }
        }
        
        pano.add(Texte);
        pano.add(rejouer);
        
        this.setContentPane(pano);
        this.pack();
        this.setTitle("Victoire");
        
        rejouer.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==rejouer) {
            rejouerCondition = true;
            this.setVisible(false);
        }
    }
    public Boolean showDialog(){ 
        this.setVisible(true); //on ouvre la fenêtre
        return rejouerCondition; //on retourne le résultat
    }
    
}
