/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package puissance_4;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 *
 * @author Maxen
 */
public class TableauDeJeuxAffichage extends JComponent implements MouseListener{
    
    private Grille gri;
    private int H ;
    private int L;
    private JLabel joueur ;
    private MaFenetre fen;

    public TableauDeJeuxAffichage( JLabel joueur, MaFenetre fen) {
        this.gri = new Grille(6, 8);
        H = gri.getHauteur();
        L = gri.getLargeur();
        this.joueur=joueur;
        this.fen= fen;
    }
    
    
    
    @Override
    public Dimension getPreferredSize() {
        //si vous voulez un composant d'une taille différente, il faut modifier les valeurs
        return new Dimension(800,500); //dimension du composant
    }
    
    
    @Override
    protected void paintComponent(Graphics g) {
        tableau(g, gri);
        
        for (int i = 0; i < gri.getHauteur(); i++) {
            for (int j = 0; j < gri.getLargeur(); j++) {
                if (gri.getGrille().get(i).get(j).getCase()==1) {
                    g.setColor(Color.red);
                    g.fillOval(j*(getPreferredSize().width/L), i*(getPreferredSize().height/H), (getPreferredSize().width/L), (getPreferredSize().height/H));
                }
                if (gri.getGrille().get(i).get(j).getCase()==2) {
                    g.setColor(Color.YELLOW);
                    g.fillOval(j*(getPreferredSize().width/L), i*(getPreferredSize().height/H), (getPreferredSize().width/L), (getPreferredSize().height/H));
                }
                
            }
        }
        
        
        
        
    }
    
    private void tableau(Graphics g, Grille gri) {
        
        for (int i = 0; i < H+1; i++) {
            g.drawLine(0, i*(getPreferredSize().height/H), getPreferredSize().width, i*(getPreferredSize().height/H));
        }
        
        for (int i = 0; i < L+1; i++) {
            g.drawLine(i*(getPreferredSize().width/L), 0, i*(getPreferredSize().width/L), getPreferredSize().height);
        }
        
        
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int X = e.getX()/(getPreferredSize().width/L);
        
        gri.setCase(X, Integer.parseInt(joueur.getText()));
                if (Integer.parseInt(joueur.getText())==1) {
                    joueur.setText("2");
                }
                else{
                    joueur.setText("1");
                }
                System.out.println(gri);
                repaint();
                int win = gri.win();
                if (win!=0) {
                    
                    WinFenetre winFen = new WinFenetre(fen, win);
                    Boolean Rejouer = winFen.showDialog();
                    if (Rejouer==false) {
                        this.setVisible(false);
                    }
                    else{
                        gri = new Grille(6, 8);
                        joueur.setText("1");
                        repaint();
                    }
                }
                CalculeBot Bot = new CalculeBot();
                System.out.println(Bot.Horizontal(gri));
                
    }

    @Override
    public void mousePressed(MouseEvent e) {
        ;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        ;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        ;
    }

    @Override
    public void mouseExited(MouseEvent e) {
        ;
    }
    
    
    
}
