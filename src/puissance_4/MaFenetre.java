/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package puissance_4;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Maxen
 */
public class MaFenetre extends JFrame {
    
    private ArrayList<JButton> bouton = new ArrayList<>();
    
    private JLabel name = new JLabel("Joueur : ");
    private JLabel joueur = new JLabel("1");
    
    TableauDeJeuxAffichage composant=new TableauDeJeuxAffichage(joueur,this);
    
    

    public MaFenetre() {
        
        
//        for (int i = 0; i < grille.getLargeur(); i++) {
//            bouton.add(new JButton(i+""));
//            bouton.get(i).addActionListener(this);
//        }
        
        GridBagConstraints gc = new GridBagConstraints();
        
        
        JPanel pano = new JPanel();
        pano.setLayout(new GridBagLayout());
        
        gc.gridx=0;
        gc.gridy=0;
        pano.add(name,gc);
        gc.gridx=1;
        pano.add(joueur,gc);
        
//        for (int i = 0; i < bouton.size(); i++) {
//            gc.gridx=i;
//            gc.gridy=1;
//            pano.add(bouton.get(i),gc);
//        }
        
        gc.gridx=0;
        gc.gridy=2;
        gc.gridwidth=2;
        pano.add(composant,gc);
        
        this.setContentPane(pano);
        this.pack();
        this.setTitle("Puissance 4");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        
        composant.addMouseListener(composant);
        
    }
    
    

//    @Override
//    public void actionPerformed(ActionEvent e) {
//        for (int i = 0; i < bouton.size(); i++) {
//            if (e.getSource()==bouton.get(i)) {
//                grille.setCase(i, Integer.parseInt(joueur.getText()));
//                if (Integer.parseInt(joueur.getText())==1) {
//                    joueur.setText("2");
//                }
//                else{
//                    joueur.setText("1");
//                }
//                System.out.println(grille);
//                composant.repaint();
//                int win = grille.win();
//                if (win!=0) {
//                    this.setVisible(false);
//                    WinFenetre winFen = new WinFenetre(win);
//                    winFen.setVisible(true);
//                }
//                CalculeBot Bot = new CalculeBot();
//                System.out.println(Bot.Horizontal(grille));
//            }
//        }
//    }
    
}
