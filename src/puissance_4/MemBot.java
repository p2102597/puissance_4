/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package puissance_4;

import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author Maxen
 */
public class MemBot {
    public Case[] mem;

    public MemBot(Case c1,Case c2,Case c3,Case c4) {
        mem = new Case[4];
        mem[0]=c1;
        mem[1]=c2;
        mem[2]=c3;
        mem[3]=c4;
    }
    
    public int nbrJ1(){
        int cpt =0;
        for (int i = 0; i < 4; i++) {
            if (mem[i].getCase()==1) {
                cpt++;
            }
        }
        return cpt;
    }
    public int nbrJ2(){
        int cpt =0;
        for (int i = 0; i < 4; i++) {
            if (mem[i].getCase()==2) {
                cpt++;
            }
        }
        return cpt;
    }
    public int nbrVide(){
        int cpt =0;
        for (int i = 0; i < 4; i++) {
            if (mem[i].getCase()==0) {
                cpt++;
            }
        }
        return cpt;
    }
    public Point ouJouer(){
        for (int i = 0; i < 4; i++) {
            if (mem[i].getCase()==0) {
                return new Point(mem[i].getX(), mem[i].getY());
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String tmp="";
        for (int i = 0; i < 4; i++) {
            tmp+=nbrJ1()+" "+nbrJ2()+" "+nbrVide()+'\n';
        }
        return tmp;
    }
    
    
}
